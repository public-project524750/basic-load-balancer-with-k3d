# Reddit post about how to make a load balancer

https://www.reddit.com/r/kubernetes/comments/18m4kvp/how_to_use_a_basic_loadbalancer_with_k3d/

# Details
## Step 1 :

Create a new cluster :
```bash
k3d cluster create my-loadbalanced-cluster -p "80:80@loadbalancer" -p "443:443@loadbalancer"
```
Explanation :
You're explicitly telling k3d to:
- Create a load balancer node for this cluster.
- Forward the local machine's ports (80 for HTTP and 443 for HTTPS) to this load balancer node inside the Kubernetes cluster.
In this case, k3d will automatically create and configure a load balancer (using Traefik by default) as part of your cluster setup, specifically because you are using the @loadbalancer port mapping.


Check that you have the loadbalancer running :
```bash
$ kubectl get services -n kube-system
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
traefik          LoadBalancer   10.43.236.49    172.20.0.2    80:30016/TCP,443:30126/TCP   277d
```

Check if this IP address can receive requests :
```bash
$ curl 172.20.0.2
404 page not found
```

This means that the loadbalancer is working.

## Step 2 :
A basic service that serves as a fix and common access point for the target deployment, and to which the loadbalancer will redirect.
An ingress to make the loadbalancer available to the outside of the cluster 
A deployment resource that will be used by the deployment controller to manage pods based on the configuration :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
            - containerPort: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    kubernetes.io/ingress.class: "traefik"
spec:
  rules:
  - host: your-hostname.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx-service
            port:
              number: 80
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

## Step 3 :
Configure the host `vim /etc/hosts` to redirect requests made on `your-hostname.example.com` to `172.20.0.2` :

```bash
172.20.0.2      your-hostname.example.com
```

## Step 4 :
Start the service :

```bash
$ kubectl apply -f ingress-nginx-clusterip.yaml
$ kubectl get deploy,po,ingress,service
```

```bash
NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   1/1     1            1           51s

NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-57d84f57dc-9v2nl   1/1     Running   0          51s

NAME                                      CLASS    HOSTS                       ADDRESS      PORTS   AGE
ingress.networking.k8s.io/nginx-ingress   <none>   your-hostname.example.com   172.20.0.2   80      51s

NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/kubernetes      ClusterIP   10.43.0.1       <none>        443/TCP   277d
service/nginx-service   ClusterIP   10.43.102.190   <none>        80/TCP    51s
```

The service is working as expected.

Now test if that nginx service is reachable from outside :

```bash
$ curl your-hostname.example.com
```

```html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
</html>
```

> Note : to ensure your kubectl is pointing to the correct cluster config use `kubectl config use-context k3d-my-loadbalanced-cluster`


# Technical details
## In k3d, Is the Load Balancer Always the Ingress Controller?
In k3d, when you create a cluster with the @loadbalancer flag, Traefik is typically set up as both the Load Balancer and Ingress Controller. However, this is a specific configuration choice made by k3d for simplicity:
- Load Balancer: Routes external traffic into the cluster.
- Ingress Controller: Manages routing rules for internal traffic using Ingress resources.

But, technically, they can be separated if you install different components manually. For example, you could install an Nginx Ingress Controller while still using Traefik or another tool for external load balancing.

## What are the differences in Cloud environments?
In cloud environments (such as AWS, GCP, Azure), when you use a LoadBalancer service in Kubernetes, it provisions an external load balancer from the cloud provider. This cloud load balancer routes external traffic into your Kubernetes cluster, typically to specific services. To manage HTTP(S) routing inside the cluster, you need to set up one or more Ingress Controllers, which handle traffic based on Ingress resources.

You can set up the NGINX or TRAEFIK Ingress Controller in a cloud environment using helm.
Example :
```bash
# Add the official NGINX ingress Helm repository:
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

# Install NGINX Ingress Controller:
helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.service.type=LoadBalancer
```

```bash
# Add the Traefik Helm chart repository:
helm repo add traefik https://helm.traefik.io/traefik
helm repo update

# Install Traefik Ingress Controller:
helm install traefik traefik/traefik --set service.type=LoadBalancer
```
